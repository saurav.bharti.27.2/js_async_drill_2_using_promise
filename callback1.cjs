

const boards = require('./data/boards_1.json')

function callback1( board_id){

    return new Promise((resolve, reject) => {

        setTimeout( () => {

                collect_information_from_boards_list(boards, board_id).then((result) => {

                    resolve(result)

                } ).catch((err)=>{

                    reject(err)

                })
                

                
            }, 3000 )
            
    })
   

}


function collect_information_from_boards_list(boards, given_board_id){

    
    return new Promise((resolve, reject) => {

        setTimeout( () => {

                try{
        
                    boards.forEach((board) => {
        
                        if(board.id === given_board_id){
        
                            resolve(board)
        
                        }
                
                    }  )
                }
                catch(err){
                
                    reject(err)
                
                }
                
            }, 2000 )
            
    } )
}


module.exports = callback1
