
const boards = require('./data/boards_1.json')

const lists = require('./data/lists_1.json')

const callback1 = require('./callback1.cjs')

const callback2 = require('./callback2.cjs')

const callback3 = require('./callback3.cjs')

function callback5( board_name, list_names ){
    
    return new Promise((resolve, reject) => {

        let array_of_promises = []

        setTimeout(() => {
    
            let get_board_id = boards.filter( (board) => {
                
                if(board.name == board_name){
    
                    return board;
    
                }
    
            } )
    
            let board_id = ''

            /// For finding information about a board id of given board name.

            array_of_promises.push( new Promise ((resolve, reject) =>{

                if(!get_board_id){

                    reject("No board found with this name")

                }else{
                    
                    board_id = get_board_id[0].id

                    callback1(board_id).then((board) => {

                        resolve(board)

                    }).catch((err) => {

                        reject(err)

                    })

                }
            })) 
    
            
            // for finding all the lists of a board it

            array_of_promises.push( new Promise((resolve, reject) => {

                callback2(board_id).then((board_data) => {
                    
                    resolve(board_data)

                } ).catch(err => {
                    
                    reject(err)

                })
                
            }))

       

            //for finding all the ids for given list names.
            
            let list_ids = list_names.reduce((previous_state, each_list_name)=> {

                Object.entries(lists).forEach(each_list_data => {

                    each_list_data[1].forEach((card) => {

                        if(card.name === each_list_name){
                            
                            previous_state.push([ card.name, card.id ])

                        }
                    })

                    
                })
                return previous_state

            }, [])

            array_of_promises.push( (new Promise(( resolve, reject ) => {

                callback3(list_ids).then((card_list) => {
                    
                    resolve(card_list)

                }).catch((err) => {

                    reject(err)

                })

            } )) )

            Promise.all(array_of_promises).then((outcome) => {

                resolve(outcome)

            }).catch(err => {
                
                reject(err)

            })

    
    
        }, 2000)


    })


}


module.exports = callback5