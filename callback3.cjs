
const cards = require('./data/cards_1.json')

const lists = require('./data/lists_1.json')

function callback3(listId){

    return new Promise((resolve, reject) => {

        setTimeout( () => {

            let good_list_with_names_and_id = [];
    
            // Only for testcase3 because, here we will be directly supplying listId not the listId name
    
            if(!Array.isArray(listId)){
    
                let given_list_id = listId
    
                Object.entries(lists).forEach(each_list_data => {
    
                    each_list_data[1].forEach((card) => {

                        if(card.id === given_list_id){
                            
                            good_list_with_names_and_id.push([ card.name, card.id ])

                        }
                    })
    
                       
                })
    
            }
            else{
    
                good_list_with_names_and_id = listId
    
            }
    
            cards_belonging_to_a_list(cards, good_list_with_names_and_id).then((card_list) => {

                resolve(card_list)

            }).catch((err) => {

                reject(err)

            })

        }, 2000 )
    

    } )
    
}

function cards_belonging_to_a_list(cards, given_list_id){

    return new Promise((resolve, reject) => {

        setTimeout( () => {

            try{
    
                let good_cards= given_list_id.reduce((previous_state, cards_to_search) => {
    
                    Object.entries(cards).forEach( ([card_id, card_list]) => {
    
                        if(card_id === cards_to_search[1]){
    
    
                            previous_state[cards_to_search[0]] = {}
    
                            previous_state[cards_to_search[0]]  = card_list
    
                        }
    
                    } )
                    return previous_state
    
                }, {})
                
                resolve(good_cards)
    
            }
            catch(err){
    
                reject(err)
    
            }
    
        }, 2000 )
    

    })
    
}

module.exports = callback3