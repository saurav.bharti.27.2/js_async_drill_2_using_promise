
const lists = require('./data/lists_1.json')

function callback2(board_id){

    return new Promise ((resolve, reject) => {

        setTimeout(() => {
    
            lists_belonging_to_a_board(lists, board_id).then((list_of_board) =>{
                
                resolve(list_of_board)

            }).catch(err => {
                
                reject(err)

            })
    
        }, 3000)

    })


}

function lists_belonging_to_a_board(lists, given_board_id){


    return new Promise((resolve, reject) => {

        setTimeout( () => {
            
            try{
                let response = {}
        
                Object.entries(lists).forEach(([board_id, list]) => {
                    
                    if(board_id == given_board_id){
                        
                        response[board_id] = list
        
                    }
            
                } )
                
                    resolve(response)
            }
            catch(err){
        
                reject(err)
        
            }
        
        }, 2000 )


    } )

}


module.exports = callback2